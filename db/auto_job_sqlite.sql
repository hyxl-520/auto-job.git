-- 删除aj_config表
DROP TABLE IF EXISTS aj_config;

-- 删除aj_job_logs表
DROP TABLE IF EXISTS aj_job_logs;

-- 删除aj_job_table_matcher表
DROP TABLE IF EXISTS aj_job_table_matcher;

-- 删除aj_method_job表
DROP TABLE IF EXISTS aj_method_job;

-- 删除aj_remote_job表
DROP TABLE IF EXISTS aj_remote_job;

-- 删除aj_run_logs表
DROP TABLE IF EXISTS aj_run_logs;

-- 删除aj_scheduling_record表
DROP TABLE IF EXISTS aj_scheduling_record;

-- 删除aj_script_job表
DROP TABLE IF EXISTS aj_script_job;

-- 删除aj_trigger表
DROP TABLE IF EXISTS aj_trigger;


-- 创建表 aj_config
CREATE TABLE IF NOT EXISTS aj_config
(
    id                 INTEGER PRIMARY KEY,
    task_id            INTEGER,
    content            TEXT,
    content_type       TEXT,
    serialization_type TEXT,
    status             INTEGER,
    write_timestamp    INTEGER,
    create_time        TEXT,
    del_flag           INTEGER
);

-- 创建表 aj_job_logs
CREATE TABLE IF NOT EXISTS aj_job_logs
(
    id              INTEGER PRIMARY KEY,
    scheduling_id   INTEGER,
    task_id         INTEGER,
    write_timestamp INTEGER,
    write_time      TEXT,
    log_level       TEXT,
    message         TEXT,
    del_flag        INTEGER
);

-- 创建表 aj_job_table_matcher
CREATE TABLE IF NOT EXISTS aj_job_table_matcher
(
    id        INTEGER PRIMARY KEY,
    task_id   INTEGER,
    task_type INTEGER,
    status    INTEGER,
    del_flag  INTEGER
);

-- 创建表 aj_method_job
CREATE TABLE IF NOT EXISTS aj_method_job
(
    id                      INTEGER PRIMARY KEY,
    alias                   TEXT,
    version_id              INTEGER,
    method_class_name       TEXT,
    method_name             TEXT,
    params                  TEXT,
    content                 TEXT,
    method_object_factory   TEXT,
    trigger_id              INTEGER,
    type                    INTEGER,
    is_child_task           INTEGER,
    is_sharding_task        INTEGER,
    run_lock                INTEGER,
    task_level              INTEGER,
    version                 INTEGER,
    running_status          INTEGER,
    belong_to               INTEGER,
    status                  INTEGER,
    executable_machines     TEXT,
    load_balancing_strategy TEXT,
    create_time             TEXT,
    del_flag                INTEGER
);

-- 创建表 aj_remote_job
CREATE TABLE IF NOT EXISTS aj_remote_job
(
    id                      INTEGER PRIMARY KEY,
    alias                   TEXT,
    version_id              INTEGER,
    connect_timeout         INTEGER,
    read_timeout            INTEGER,
    write_timeout           INTEGER,
    host                    TEXT,
    port                    INTEGER,
    uri                     TEXT,
    protocol_type           TEXT,
    req_type                TEXT,
    header                  TEXT,
    body                    TEXT,
    params                  TEXT,
    interceptor             TEXT,
    trigger_id              INTEGER,
    type                    INTEGER,
    is_child_task           INTEGER,
    is_sharding_task        INTEGER,
    run_lock                INTEGER,
    task_level              INTEGER,
    version                 INTEGER,
    running_status          INTEGER,
    belong_to               INTEGER,
    status                  INTEGER,
    executable_machines     TEXT,
    load_balancing_strategy TEXT,
    create_time             TEXT,
    del_flag                INTEGER
);

-- 创建表 aj_run_logs
CREATE TABLE IF NOT EXISTS aj_run_logs
(
    id              INTEGER PRIMARY KEY,
    scheduling_id   INTEGER,
    task_id         INTEGER,
    task_type       TEXT,
    run_status      INTEGER,
    schedule_times  INTEGER,
    message         TEXT,
    result          TEXT,
    error_stack     TEXT,
    write_timestamp INTEGER,
    write_time      TEXT,
    del_flag        INTEGER
);

-- 创建表 aj_scheduling_record
CREATE TABLE IF NOT EXISTS aj_scheduling_record
(
    id                INTEGER PRIMARY KEY,
    write_timestamp   INTEGER,
    scheduling_time   TEXT,
    task_alias        TEXT,
    task_id           INTEGER,
    is_success        INTEGER,
    is_run            INTEGER,
    scheduling_type   INTEGER,
    sharding_id       INTEGER,
    executing_machine TEXT,
    result            TEXT,
    execution_time    INTEGER,
    del_flag          INTEGER
);

-- 创建表 aj_script_job
CREATE TABLE IF NOT EXISTS aj_script_job
(
    id                      INTEGER PRIMARY KEY,
    alias                   TEXT,
    version_id              INTEGER,
    params                  TEXT,
    script_content          TEXT,
    script_path             TEXT,
    script_file_name        TEXT,
    script_cmd              TEXT,
    trigger_id              INTEGER,
    type                    INTEGER,
    is_child_task           INTEGER,
    is_sharding_task        INTEGER,
    run_lock                INTEGER,
    task_level              INTEGER,
    version                 INTEGER,
    running_status          INTEGER,
    belong_to               INTEGER,
    status                  INTEGER,
    executable_machines     TEXT,
    load_balancing_strategy TEXT,
    create_time             TEXT,
    del_flag                INTEGER
);

-- 创建表 aj_trigger
CREATE TABLE IF NOT EXISTS aj_trigger
(
    id                     INTEGER PRIMARY KEY,
    cron_expression        TEXT,
    last_run_time          INTEGER,
    last_triggering_time   INTEGER,
    next_triggering_time   INTEGER,
    is_last_success        INTEGER,
    repeat_times           INTEGER,
    finished_times         INTEGER,
    current_repeat_times   INTEGER,
    cycle                  INTEGER,
    task_id                INTEGER,
    child_tasks_id         TEXT,
    maximum_execution_time INTEGER,
    is_run                 INTEGER,
    is_pause               INTEGER,
    create_time            TEXT,
    del_flag               INTEGER
);

-- Create index idx_task_id on table aj_trigger
CREATE INDEX IF NOT EXISTS idx_task_id ON aj_trigger (task_id);

-- Create index idx_schedule on table aj_trigger
CREATE INDEX IF NOT EXISTS idx_schedule ON aj_trigger (next_triggering_time);

-- Create index idx_task_id on table aj_scheduling_record
CREATE INDEX IF NOT EXISTS idx_task_id ON aj_scheduling_record (task_id);

-- Create index idx_task_id_s_id on table aj_run_logs
CREATE INDEX IF NOT EXISTS idx_task_id_s_id ON aj_run_logs (task_id, scheduling_id);

-- Create index idx_schedule on table aj_remote_job
CREATE INDEX IF NOT EXISTS idx_schedule ON aj_remote_job (version_id);

-- Create index idx_schedule on table aj_method_job
CREATE INDEX IF NOT EXISTS idx_schedule ON aj_method_job (version_id);

-- Create index idx_task_id_status_del on table aj_job_table_matcher
CREATE INDEX IF NOT EXISTS idx_task_id_status_del ON aj_job_table_matcher (task_id, status, del_flag);

-- Create index idx_task_id_s_id on table aj_job_logs
CREATE INDEX IF NOT EXISTS idx_task_id_s_id ON aj_job_logs (task_id, scheduling_id);

-- Create index idx_task_id on table aj_config
CREATE INDEX IF NOT EXISTS idx_task_id ON aj_config (task_id);
