package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;

/**
 * 负载均衡策略的抽象接口
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-19 14:02
 * @email 1158055613@qq.com
 */
public interface LoadBalancingStrategy {
    default void beforeChoose(List<ClusterNode> solves) {

    }

    ClusterNode choose(List<ClusterNode> solves, AutoJobTask task);

    default void afterChoose(ClusterNode chosen, List<ClusterNode> solves) {

    }
}
