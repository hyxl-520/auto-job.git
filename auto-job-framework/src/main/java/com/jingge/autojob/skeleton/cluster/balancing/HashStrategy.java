package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-19 16:04
 * @email 1158055613@qq.com
 */
public class HashStrategy implements LoadBalancingStrategy {
    @Override
    public ClusterNode choose(List<ClusterNode> solves, AutoJobTask task) {
        if (task == null) {
            return null;
        }
        int pos = Math.abs(task.hashCode() % solves.size());
        return pos < solves.size() ? solves.get(pos) : null;
    }
}
