package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-19 15:26
 * @email 1158055613@qq.com
 */
public class PollingStrategy implements LoadBalancingStrategy {
    private static final AtomicInteger pos = new AtomicInteger(-1);

    @Override
    public ClusterNode choose(List<ClusterNode> solves, AutoJobTask task) {
        if (solves == null) {
            return null;
        }
        if (pos.incrementAndGet() >= solves.size()) {
            pos.set(0);
        }
        //System.out.println("当前从机数" + solves.size() + ",当前pos=" + pos);
        return solves.size() > 0 ? solves.get(pos.get()) : null;
    }
}
