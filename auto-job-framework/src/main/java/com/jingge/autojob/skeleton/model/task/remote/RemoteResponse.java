package com.jingge.autojob.skeleton.model.task.remote;

import com.google.gson.JsonObject;
import com.jingge.autojob.util.convert.StringUtils;
import com.jingge.autojob.util.json.JsonUtil;

import java.io.InputStream;

/**
 * 远程任务响应
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-04 11:28
 * @email 1158055613@qq.com
 */
public interface RemoteResponse {
    boolean isSuccess();

    default String getDigest() {
        String res = getAsString();
        if (StringUtils.isEmpty(res)) {
            return "无法预览响应摘要";
        }
        return res.length() >= 200 ? res.substring(0, 200) : res;
    }

    default JsonObject getAsJsonObject() {
        return JsonUtil.stringToJsonObj(getAsString());
    }

    long getResponseLength();

    InputStream getAsInputStream();

    <T> T getAsObject(Class<T> type);

    String getAsString();

    Object getAsObject();
}
