package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-20 16:56
 * @email 1158055613@qq.com
 */
public class MinimumDelayStrategy implements LoadBalancingStrategy {
    @Override
    public ClusterNode choose(List<ClusterNode> solves, AutoJobTask task) {
        int pos = 0;
        long max = 0;
        for (int i = 0; i < solves.size(); i++) {
            if (solves
                    .get(i)
                    .getLastResponseTime() != null && solves
                    .get(i)
                    .getLastResponseTime() > max) {
                max = solves
                        .get(i)
                        .getLastResponseTime();
                pos = i;
            }
        }
        return solves.get(pos);
    }
}
