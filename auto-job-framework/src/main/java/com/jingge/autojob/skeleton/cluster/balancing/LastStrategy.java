package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-20 16:47
 * @email 1158055613@qq.com
 */
public class LastStrategy implements LoadBalancingStrategy {
    @Override
    public ClusterNode choose(List<ClusterNode> solves, AutoJobTask task) {
        return solves.size() > 0 ? solves.get(solves.size() - 1) : null;
    }
}
