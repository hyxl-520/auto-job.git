package com.jingge.autojob.skeleton.db.mapper;

import com.jingge.autojob.api.task.params.RemoteTaskEditParams;
import com.jingge.autojob.api.task.params.TaskEditParams;
import com.jingge.autojob.skeleton.db.StorageChain;
import com.jingge.autojob.skeleton.db.TransactionManager;
import com.jingge.autojob.skeleton.db.entity.AutoJobRemoteTaskEntity;
import com.jingge.autojob.skeleton.db.task.*;
import com.jingge.autojob.skeleton.enumerate.SaveStrategy;
import com.jingge.autojob.skeleton.framework.config.DBTableConstant;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.util.id.IdGenerator;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 15:28
 * @email 1158055613@qq.com
 */
public class AutoJobRemoteTaskEntityMapper extends AutoJobTaskEntityBaseMapper<AutoJobRemoteTaskEntity> {
    private static final String ALL_COLUMNS = "connect_timeout, read_timeout, write_timeout, host, port, uri, protocol_type, req_type, header, body, interceptor";

    public AutoJobRemoteTaskEntityMapper() {
        super(AutoJobRemoteTaskEntity.class);
    }

    @Override
    public boolean saveTasks(List<AutoJobTask> tasks) {
        List<Long> ids = filterExistTask(SaveStrategy.SAVE_IF_ABSENT.filterTasks(tasks));
        tasks.removeIf(item -> ids.contains(item.getVersionId()));
        TransactionManager.openTransaction(dataSourceHolder, Connection.TRANSACTION_SERIALIZABLE);
        deleteExistTaskByVersionID(SaveStrategy.UPDATE.filterTasks(tasks));
        boolean flag = new StorageChain.Builder<AutoJobTask>()
                .addNode(new TriggerStorageNode())
                .addNode(new TryConfigStorageNode())
                .addNode(new MailConfigStorageNode())
                .addNode(new ShardingConfigStorageNode())
                .addNode(new RemoteTaskStorageNode())
                .build()
                .store(tasks, Connection.TRANSACTION_SERIALIZABLE);
        if (flag) {
            List<AutoJobMapperHolder.TaskTypeMatcher> matchers = tasks
                    .stream()
                    .map(task -> {
                        AutoJobMapperHolder.TaskTypeMatcher taskTypeMatcher = new AutoJobMapperHolder.TaskTypeMatcher();
                        taskTypeMatcher.taskType = 2;
                        taskTypeMatcher.delFlag = 0;
                        taskTypeMatcher.id = IdGenerator.getNextIdAsLong();
                        taskTypeMatcher.status = 1;
                        taskTypeMatcher.taskID = task.getId();
                        return taskTypeMatcher;
                    })
                    .collect(Collectors.toList());
            AutoJobMapperHolder.TASK_TYPE_MATCHER_MAPPER.insertList(matchers);
        }
        return flag;
    }

    @Override
    public int updateById(TaskEditParams editParams, long taskId) {
        if (editParams == null) {
            return -1;
        }
        AutoJobRemoteTaskEntity entity = new AutoJobRemoteTaskEntity();
        entity.setAlias(editParams.getAlias());
        entity.setBelongTo(editParams.getBelongTo());
        entity.setTaskLevel(editParams.getTaskLevel());
        if (editParams instanceof RemoteTaskEditParams) {
            RemoteTaskEditParams remoteTaskEditParams = (RemoteTaskEditParams) editParams;
            entity.setInterceptor(remoteTaskEditParams.getInterceptor());
            entity.setPort(remoteTaskEditParams.getPort());
            entity.setHost(remoteTaskEditParams.getHost());
            entity.setUri(remoteTaskEditParams.getUri());
            entity.setHeader(remoteTaskEditParams.getHeader());
            entity.setBody(remoteTaskEditParams.getBody());
        }
        return updateEntity(entity, "id = ?", taskId);
    }

    @Override
    public String getAllColumns() {
        return BASE_COLUMNS + ", " + ALL_COLUMNS;
    }

    @Override
    public String getTableName() {
        return DBTableConstant.REMOTE_TASK_TABLE_NAME;
    }
}
