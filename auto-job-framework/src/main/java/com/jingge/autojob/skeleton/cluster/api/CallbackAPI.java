package com.jingge.autojob.skeleton.cluster.api;

import com.jingge.autojob.skeleton.annotation.AutoJobRPCClient;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

/**
 * 执行回调API
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-22 10:13
 * @email 1158055613@qq.com
 */
@AutoJobRPCClient("defaultCallbackAPI")
public interface CallbackAPI {
    Boolean refreshed(AutoJobTask refreshedTask);
}
