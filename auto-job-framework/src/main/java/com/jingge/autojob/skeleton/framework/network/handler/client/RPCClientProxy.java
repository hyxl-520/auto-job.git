package com.jingge.autojob.skeleton.framework.network.handler.client;

import com.jingge.autojob.util.servlet.InetUtil;

import java.lang.reflect.Proxy;
import java.util.concurrent.TimeUnit;

/**
 * 客户端代理类
 *
 * @Author Huang Yongxiang
 * @Date 2022/09/19 16:40
 */
@SuppressWarnings("unchecked")
public class RPCClientProxy<T> {
    private final RPCClientInvokeProxy invokeProxy;
    private final Class<T> proxyInterface;
    private volatile T proxyInstance;

    public RPCClientProxy(String host, int port, Class<T> proxyInterface) {
        this.proxyInterface = proxyInterface;
        this.invokeProxy = new RPCClientInvokeProxy("localhost".equals(host) ? InetUtil.getLocalhostIp() : host, port, proxyInterface);
    }

    /**
     * 创建目标接口的代理，如果已经创建则直接返回
     *
     * @return T
     * @author Huang Yongxiang
     * @date 2022/11/1 15:05
     */
    public T clientProxy() {
        if (proxyInstance == null) {
            synchronized (RPCClientProxy.class) {
                if (proxyInstance == null) {
                    proxyInstance = (T) Proxy.newProxyInstance(proxyInterface.getClassLoader(), new Class<?>[]{proxyInterface}, invokeProxy);
                }
            }
        }
        return proxyInstance;
    }

    public T clientProxy(long connectTimeout, long readTimeout, TimeUnit unit) {
        invokeProxy
                .setConnectTimeout(connectTimeout, unit)
                .setReadTimeout(readTimeout, unit);
        if (proxyInstance == null) {
            synchronized (RPCClientProxy.class) {
                if (proxyInstance == null) {
                    proxyInstance = (T) Proxy.newProxyInstance(proxyInterface.getClassLoader(), new Class<?>[]{proxyInterface}, invokeProxy);
                }
            }
        }
        return proxyInstance;
    }

    public void destroyProxy() {
        invokeProxy.close();
    }
}
