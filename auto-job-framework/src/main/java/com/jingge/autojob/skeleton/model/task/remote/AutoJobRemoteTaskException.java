package com.jingge.autojob.skeleton.model.task.remote;

import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.skeleton.lang.AutoJobException;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-12 14:43
 * @email 1158055613@qq.com
 */
public class AutoJobRemoteTaskException extends AutoJobException {
    public AutoJobRemoteTaskException(AutoJobTask task, String message) {
        super(task, message);
    }

    public AutoJobRemoteTaskException() {
        super();
    }

    public AutoJobRemoteTaskException(String message) {
        super(message);
    }

    public AutoJobRemoteTaskException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutoJobRemoteTaskException(Throwable cause) {
        super(cause);
    }

    protected AutoJobRemoteTaskException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
