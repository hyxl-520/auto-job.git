package com.jingge.autojob.skeleton.db.entity;

import com.jingge.autojob.skeleton.db.mapper.AutoJobMapperHolder;
import com.jingge.autojob.skeleton.enumerate.LoadBalancingEnum;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.skeleton.model.task.remote.RemoteTask;
import com.jingge.autojob.util.convert.DefaultValueUtil;
import com.jingge.autojob.util.json.JsonUtil;

import java.sql.Timestamp;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 14:59
 * @email 1158055613@qq.com
 */
public class RemoteTask2EntityConvertor implements Task2EntityConvertor<AutoJobRemoteTaskEntity> {
    @Override
    public AutoJobRemoteTaskEntity convert(AutoJobTask source) {
        AutoJobRemoteTaskEntity entity = new AutoJobRemoteTaskEntity();
        entity.setId(source.getId());
        entity.setType(2);
        RemoteTask remoteTask = (RemoteTask) source;
        if (remoteTask.getBody() != null) {
            entity.setBody(remoteTask.getBody() instanceof String ? (String) remoteTask.getBody() : JsonUtil.pojoToJsonString(remoteTask.getBody()));
        }
        if (remoteTask.getHeader() != null) {
            entity.setHeader(remoteTask.getHeader() instanceof String ? (String) remoteTask.getHeader() : JsonUtil.pojoToJsonString(remoteTask.getHeader()));
        }
        entity.setReqType(remoteTask.getReqType());
        entity.setUri(remoteTask.getUri());
        entity.setHost(remoteTask.getHost());
        entity.setPort(remoteTask.getPort());
        if (remoteTask.getProtocolType() != null) {
            entity.setProtocolType(remoteTask
                    .getProtocolType()
                    .getType());
        }
        if (remoteTask.getInterceptor() != null) {
            entity.setInterceptor(remoteTask
                    .getInterceptor()
                    .getClass()
                    .getName());
        }
        if (source.getExecutableMachines() != null && source
                .getExecutableMachines()
                .size() > 0) {
            entity.setExecutableMachines(String.join(",", source.getExecutableMachines()));
        }
        if (source.getVersionId() != null) {
            AutoJobRemoteTaskEntity latestVersion = AutoJobMapperHolder.REMOTE_TASK_ENTITY_MAPPER.selectLatestAnnotationTask(source.getVersionId());
            if (latestVersion != null && latestVersion.getVersion() != null) {
                entity.setVersion(latestVersion.getVersion() + 1);
            } else {
                entity.setVersion(0L);
            }
            entity.setVersionId(source.getVersionId());
        }
        entity.setBelongTo(DefaultValueUtil.defaultValue(source.getBelongTo(), -1L));
        if (source.getTrigger() != null) {
            entity.setTriggerId(source
                    .getTrigger()
                    .getTriggerId());
        }
        entity.setRunningStatus(source
                .getRunningStatus()
                .getFlag());
        entity.setLoadBalancingStrategy(source.getLoadBalancing() == null ? LoadBalancingEnum.POLLING.getName() : source
                .getLoadBalancing()
                .getName());
        entity.setConnectTimeout(remoteTask.getConnectTimeout());
        entity.setReadTimeout(remoteTask.getReadTimeout());
        entity.setWriteTimeout(remoteTask.getWriteTimeout());
        entity.setRunLock(0);
        entity.setStatus(1);
        entity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        entity.setAlias(source.getAlias());
        entity.setIsChildTask(source.getIsChildTask() != null && source.getIsChildTask() ? 1 : 0);
        entity.setIsShardingTask(source.getIsShardingTask() != null && source.getIsShardingTask() ? 1 : 0);
        entity.setTaskLevel(source.getTaskLevel());
        entity.setParams(source.getParamsString());
        return entity;
    }
}
