package com.jingge.autojob.skeleton.cluster.balancing;

import com.jingge.autojob.skeleton.cluster.model.ClusterNode;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;
import java.util.Random;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-19 16:15
 * @email 1158055613@qq.com
 */
public class RandomStrategy implements LoadBalancingStrategy {
    @Override
    public ClusterNode choose(List<ClusterNode> solves, AutoJobTask task) {
        Random random = new Random();
        int pos = random.nextInt(solves.size() + 1);
        return solves.get(pos);
    }
}
