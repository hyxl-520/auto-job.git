package com.jingge.autojob.skeleton.db.entity;

import com.jingge.autojob.skeleton.db.mapper.AutoJobMapperHolder;
import com.jingge.autojob.skeleton.enumerate.LoadBalancingEnum;
import com.jingge.autojob.skeleton.enumerate.RemoteProtocolType;
import com.jingge.autojob.skeleton.framework.task.AutoJobRunningStatus;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.skeleton.model.interpreter.AutoJobAttributeContext;
import com.jingge.autojob.skeleton.model.task.remote.RemoteTask;
import com.jingge.autojob.skeleton.model.task.remote.RemoteTaskInterceptor;
import com.jingge.autojob.util.bean.ObjectUtil;
import com.jingge.autojob.util.convert.DefaultValueUtil;
import com.jingge.autojob.util.convert.StringUtils;

import java.util.Arrays;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 15:00
 * @email 1158055613@qq.com
 */
public class RemoteEntity2TaskConvertor implements Entity2TaskConvertor<AutoJobRemoteTaskEntity> {
    @Override
    public AutoJobTask convert(AutoJobRemoteTaskEntity source) {
        if (ObjectUtil.isNull(source)) {
            return null;
        }
        AutoJobTriggerEntity triggerEntity = AutoJobMapperHolder.TRIGGER_ENTITY_MAPPER.selectOneByTaskId(source.getId());
        if (source.getType() == 2) {
            RemoteTask task = new RemoteTask();
            task.setId(source.getId());
            task.setTrigger(EntityConvertor.triggerEntity2Trigger(triggerEntity));
            task.setTaskLevel(source.getTaskLevel());
            task.setIsChildTask(source.getIsChildTask() != null && source.getIsChildTask() == 1);
            task.setBelongTo(source.getBelongTo());
            task.setIsShardingTask(source.getIsShardingTask() != null && source.getIsShardingTask() == 1);
            task.setVersionId(source.getVersionId());
            task.setType(AutoJobTask.TaskType.DB_TASK);
            task.setIsAllowRegister(true);
            task.setAlias(source.getAlias());
            task.setIsFinished(false);
            if (!StringUtils.isEmpty(source.getExecutableMachines())) {
                String[] machine = source
                        .getExecutableMachines()
                        .split(",");
                task.setExecutableMachines(Arrays.asList(machine));
            }
            if (source.getRunningStatus() != null) {
                task.setRunningStatus(AutoJobRunningStatus.findByFlag(source.getRunningStatus()));
                if (task.getTrigger() != null) {
                    task
                            .getTrigger()
                            .setIsRetrying(task.getRunningStatus() == AutoJobRunningStatus.RETRYING);
                }
            }
            task.setParamsString(source.getParams());
            if (!StringUtils.isEmpty(source.getParams())) {
                task.setParams(new AutoJobAttributeContext(task).getAttributeEntity());
            }
            task.setLoadBalancing(LoadBalancingEnum.findByName(source.loadBalancingStrategy));
            task.setConnectTimeout(DefaultValueUtil.defaultValue(source.getConnectTimeout(), 10L));
            task.setWriteTimeout(DefaultValueUtil.defaultValue(source.getWriteTimeout(), 30L));
            task.setReadTimeout(DefaultValueUtil.defaultValue(source.getReadTimeout(), 30L));
            task.setBody(source.getBody());
            task.setHeader(source.getHeader());
            task.setHost(source.getHost());
            task.setInterceptor((RemoteTaskInterceptor) ObjectUtil.getClassInstanceObject(ObjectUtil.classPath2Class(source.getInterceptor())));
            task.setReqType(source.getReqType());
            task.setProtocolType(RemoteProtocolType.findByType(source.getProtocolType()));
            task.setUri(source.getUri());
            task.setPort(source.getPort());
            return task;
        }

        return null;
    }
}
