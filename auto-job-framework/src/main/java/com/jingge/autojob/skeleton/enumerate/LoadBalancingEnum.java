package com.jingge.autojob.skeleton.enumerate;

import com.jingge.autojob.skeleton.cluster.balancing.*;
import com.jingge.autojob.util.convert.StringUtils;

/**
 * 负载均衡策略
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-19 15:01
 * @email 1158055613@qq.com
 */
public enum LoadBalancingEnum {
    /**
     * 轮询
     */
    POLLING("polling") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new PollingStrategy();
        }
    },
    /**
     * 一致性Hash
     */
    HASH("hash") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new HashStrategy();
        }
    },
    /**
     * 随机
     */
    RANDOM("random") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new RandomStrategy();
        }
    },
    /**
     * 第一个
     */
    FIRST("first") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new FirstStrategy();
        }
    },
    /**
     * 最后一个
     */
    LAST("last") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new LastStrategy();
        }
    },
    /**
     * 最低网络时延
     */
    MINIMUM_DELAY("minimumDelay") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new MinimumDelayStrategy();
        }
    },
    /**
     * 最低负载
     */
    MINIMUM_LOAD("minimumLoad") {
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new MinimumLoadStrategy();
        }
    },
    DOWNGRADING("tierDown"){
        @Override
        public LoadBalancingStrategy getLoadBalancingStrategy() {
            return new DowngradingStrategy();
        }
    };
    protected final String name;

    LoadBalancingEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static LoadBalancingEnum findByName(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        for (LoadBalancingEnum l : values()) {
            if (l.name.equalsIgnoreCase(name)) {
                return l;
            }
        }
        return null;
    }

    public LoadBalancingStrategy getLoadBalancingStrategy() {
        throw new UnsupportedOperationException("不支持的负载均衡策略");
    }
}
