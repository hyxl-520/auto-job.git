package com.jingge.autojob.skeleton.model.task.remote;

import com.jingge.autojob.skeleton.framework.task.AutoJobRunResult;

/**
 * 远程任务拦截器
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 14:22
 * @email 1158055613@qq.com
 */
public interface RemoteTaskInterceptor {
    /**
     * 执行前根据任务信息创建对应的请求处理器
     *
     * @param task 远程任务
     * @return com.jingge.autojob.skeleton.model.task.remote.RequestHandler
     * @author JingGe(* ^ ▽ ^ *)
     * @date 2023/9/5 14:56
     */
    RequestHandler before(RemoteTask task);

    /**
     * 请求执行完成后对执行结果进行封装
     *
     * @param task           远程任务
     * @param remoteResponse 响应
     * @return com.jingge.autojob.skeleton.framework.task.AutoJobRunResult
     * @author JingGe(* ^ ▽ ^ *)
     * @date 2023/9/5 14:56
     */
    AutoJobRunResult after(RemoteTask task, RemoteResponse remoteResponse);
}
