package com.jingge.autojob.skeleton.lang;

import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.util.message.MessageManager;

/**
 * AutoJob的父级异常
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-08-15 10:11
 * @email 1158055613@qq.com
 */
public class AutoJobException extends RuntimeException {
    private AutoJobTask task;

    public AutoJobException(AutoJobTask task, String message) {
        super(message);
        this.task = task;
    }

    public AutoJobException() {
        super();
    }

    public AutoJobException(String message) {
        super(message);
    }

    public AutoJobException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutoJobException(Throwable cause) {
        super(cause);
    }

    protected AutoJobException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getMessage() {
        return task != null ? MessageManager.formatMsgLikeSlf4j("任务：{}-{}在调度过程中发生异常：{}", task.getId(), task
                .getClass()
                .getSimpleName(), super.getMessage()) : super.getMessage();
    }


}
