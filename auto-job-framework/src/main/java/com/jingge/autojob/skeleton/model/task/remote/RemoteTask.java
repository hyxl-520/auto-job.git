package com.jingge.autojob.skeleton.model.task.remote;

import com.jingge.autojob.skeleton.enumerate.RemoteProtocolType;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.skeleton.model.task.TaskExecutable;
import com.jingge.autojob.util.message.MessageManager;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 远程任务
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-04 11:09
 * @email 1158055613@qq.com
 */
@Getter
@Setter
@Accessors(chain = true)
public class RemoteTask extends AutoJobTask {
    /**
     * 请求类型
     */
    private String reqType;

    /**
     * 建立连接超时时间
     */
    private long connectTimeout;
    
    /**
     * 获取数据的超时时间
     */
    private long readTimeout;
    
    /**
     * 写入数据的超时时间
     */
    private long writeTimeout;
    
    /**
     * 远程主机地址
     */
    private String host;
    /**
     * 远程端口号
     */
    private int port;
    /**
     * uri
     */
    private String uri;
    /**
     * 拦截器
     */
    private RemoteTaskInterceptor interceptor;
    /**
     * 协议类型
     */
    private RemoteProtocolType protocolType;
    /**
     * 请求头
     */
    private Object header;
    /**
     * 请求体
     */
    private Object body;
    /**
     * 请求处理器
     */
    private RequestHandler requestHandler;

    private transient RemoteExecutable executable;

    public RemoteTask() {
    }

    @Override
    public TaskExecutable getExecutable() {
        if (executable != null) {
            return executable;
        }
        executable = new RemoteExecutable(this);
        return executable;
    }

    public RequestHandler getRequestHandler() {
        return requestHandler;
    }

    public RemoteTask setRequestHandler(RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
        return this;
    }

    @Override
    public String getReference() {
        return MessageManager.formatMsgLikeSlf4j("{}:{}:{}-{}", host, port, protocolType, id);
    }
}
