package com.jingge.autojob.skeleton.enumerate;

/**
 * 远程任务协议类型
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 14:34
 * @email 1158055613@qq.com
 */
public enum RemoteProtocolType {
    HTTP("http"), HTTPS("https");
    private final String type;

    public String getType() {
        return type;
    }

    public static RemoteProtocolType findByType(String type) {
        for (RemoteProtocolType t : values()) {
            if (t.type.equalsIgnoreCase(type)) {
                return t;
            }
        }
        return null;
    }

    RemoteProtocolType(String type) {
        this.type = type;
    }
}
