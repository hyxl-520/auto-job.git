package com.jingge.autojob.skeleton.db.task;

import com.jingge.autojob.skeleton.db.StorageNode;
import com.jingge.autojob.skeleton.db.entity.AutoJobMethodTaskEntity;
import com.jingge.autojob.skeleton.db.entity.AutoJobRemoteTaskEntity;
import com.jingge.autojob.skeleton.db.entity.EntityConvertor;
import com.jingge.autojob.skeleton.db.mapper.AutoJobMapperHolder;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 15:30
 * @email 1158055613@qq.com
 */
public class RemoteTaskStorageNode  implements StorageNode<AutoJobTask> {
    @Override
    public int store(List<AutoJobTask> entities) {
        return AutoJobMapperHolder.REMOTE_TASK_ENTITY_MAPPER.insertList(entities
                .stream()
                .map(task -> (AutoJobRemoteTaskEntity) EntityConvertor.task2Entity(task))
                .collect(Collectors.toList()));
    }
}
