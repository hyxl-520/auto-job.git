package com.jingge.autojob.skeleton.annotation;

import com.jingge.autojob.skeleton.framework.config.RetryStrategy;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 重试配置
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-14 14:04
 * @email 1158055613@qq.com
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface RetryConfig {
    /**
     * 是否使用配置文件配置的默认重试配置，当此值为true时该注解的配置不会生效
     */
    boolean useDefault() default true;

    boolean enable() default false;

    RetryStrategy retryStrategy() default RetryStrategy.LOCAL_RETRY;

    int retryTimes() default 3;

    long interval() default 5;
}
