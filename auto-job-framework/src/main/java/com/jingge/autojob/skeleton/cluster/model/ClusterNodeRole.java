package com.jingge.autojob.skeleton.cluster.model;

import com.jingge.autojob.util.convert.StringUtils;

/**
 * 节点角色
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-18 9:30
 * @email 1158055613@qq.com
 */
public enum ClusterNodeRole {
    SLAVE("slave"), MASTER("master"), EQUAL("equal");

    private final String name;

    ClusterNodeRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ClusterNodeRole findByName(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        for (ClusterNodeRole r : values()) {
            if (r.name.equalsIgnoreCase(name.trim())) {
                return r;
            }
        }
        return null;
    }
}
