package com.jingge.autojob.skeleton.model.task.remote;

/**
 * 请求处理
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-04 11:25
 * @email 1158055613@qq.com
 */
public interface RequestHandler {
    /**
     * 发送请求并且获得响应，子类必须实现该方法
     *
     * @return com.jingge.autojob.skeleton.model.task.remote.RemoteResponse
     * @author JingGe(* ^ ▽ ^ *)
     * @date 2023/9/12 14:59
     */
    RemoteResponse sendRequest();

    /**
     * 包含请求发送和响应校验的请求处理方法，子类无需覆盖
     *
     * @return com.jingge.autojob.skeleton.model.task.remote.RemoteResponse
     * @author JingGe(* ^ ▽ ^ *)
     * @date 2023/9/12 15:00
     */
    default RemoteResponse handleRequest() throws AutoJobRemoteTaskException {
        RemoteResponse remoteResponse = sendRequest();
        if (!verifyResponse(remoteResponse)) {
            throw new AutoJobRemoteTaskException("响应校验不通过，响应摘要：" + remoteResponse.getDigest());
        }
        return remoteResponse;
    }

    /**
     * 响应校验，校验不通过返回false，该任务会中断执行并且按照重试策略进行重试
     *
     * @param remoteResponse 远程响应
     * @return boolean
     * @author JingGe(* ^ ▽ ^ *)
     * @date 2023/9/12 15:01
     */
    default boolean verifyResponse(RemoteResponse remoteResponse) throws AutoJobRemoteTaskException {
        return true;
    }
}
