package com.jingge.autojob.skeleton.model.register;

import com.jingge.autojob.skeleton.framework.boot.AutoJobApplication;
import com.jingge.autojob.skeleton.framework.network.handler.server.ServiceFactory;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-18 9:25
 * @email 1158055613@qq.com
 */
public class RemoteRegisterFactory implements ServiceFactory {
    @Override
    public IAutoJobRegister newService(Class<?> service) {
        return AutoJobApplication
                .getInstance()
                .getRegister();
    }
}
