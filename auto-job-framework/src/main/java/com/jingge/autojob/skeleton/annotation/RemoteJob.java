package com.jingge.autojob.skeleton.annotation;

import com.jingge.autojob.skeleton.enumerate.LoadBalancingEnum;
import com.jingge.autojob.skeleton.enumerate.RemoteProtocolType;
import com.jingge.autojob.skeleton.enumerate.SaveStrategy;
import com.jingge.autojob.skeleton.framework.task.AutoJobTask;
import com.jingge.autojob.skeleton.model.executor.DefaultMethodObjectFactory;
import com.jingge.autojob.skeleton.model.executor.IMethodObjectFactory;
import com.jingge.autojob.skeleton.model.task.remote.BaseJsonHttpInterceptor;
import com.jingge.autojob.skeleton.model.task.remote.RemoteTaskInterceptor;

import java.lang.annotation.*;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-12 14:32
 * @email 1158055613@qq.com
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RemoteJob {
    /**
     * 拦截器
     */
    Class<? extends RemoteTaskInterceptor> interceptor() default BaseJsonHttpInterceptor.class;

    /**
     * 请求类型
     */
    String reqType() default "GET";

    /**
     * 别名
     */
    String alias() default "Empty";

    /**
     * 协议类型
     */
    RemoteProtocolType protocolType() default RemoteProtocolType.HTTP;

    /**
     * 任务类型
     */
    AutoJobTask.TaskType taskType() default AutoJobTask.TaskType.MEMORY_TASk;

    /**
     * 负载均衡策略
     */
    LoadBalancingEnum loadBalancingStrategy() default LoadBalancingEnum.POLLING;

    /**
     * 保存策略
     */
    SaveStrategy saveStrategy() default SaveStrategy.SAVE_IF_ABSENT;

    /**
     * 版本ID，等同于{@link AutoJob}里面的{@link AutoJob#id()}
     */
    long versionID() default -1;

    /**
     * 子任务ID，可以是版本ID，也可以是任务ID；多个逗号分割.
     */
    String childTasksId() default "";

    /**
     * 方法所在对象的工厂
     */
    Class<? extends IMethodObjectFactory> methodObjectFactory() default DefaultMethodObjectFactory.class;

    /**
     * 分片配置
     */
    ShardingConfig shardingConfig() default @ShardingConfig;

    /**
     * 重试配置
     */
    RetryConfig retryConfig() default @RetryConfig;
}
