package com.jingge.autojob.api.task.params;

import lombok.Getter;
import lombok.Setter;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-05 15:46
 * @email 1158055613@qq.com
 */
@Getter
@Setter
public class RemoteTaskEditParams extends TaskEditParams{
    /**
     * 建立连接超时时间
     */
    private Long connectTimeout;
    /**
     * 读取数据超时时间
     */
    private Long readTimeout;
    /**
     * 写入数据超时时间
     */
    private Long writeTimeout;
    /**
     * 远程主机地址
     */
    private String host;
    /**
     * 远程端口号
     */
    private Integer port;
    /**
     * uri
     */
    private String uri;
    /**
     * 拦截器
     */
    private String interceptor;
    /**
     * 请求头
     */
    private String header;
    /**
     * 请求体
     */
    private String body;
}
