package com.jingge.autojob.api.manage;

import com.jingge.autojob.skeleton.annotation.AutoJobRPCClient;
import com.jingge.autojob.skeleton.cluster.model.ClusterNode;

import java.util.List;

/**
 * AutoJob的管理的API
 *
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-08-31 16:32
 * @email 1158055613@qq.com
 */
@AutoJobRPCClient("AutoJobManageAPI")
public interface AutoJobManageAPI {
    List<ClusterNode> getClusterNodes();


}
