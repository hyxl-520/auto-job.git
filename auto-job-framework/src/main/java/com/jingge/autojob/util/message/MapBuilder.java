package com.jingge.autojob.util.message;

import com.jingge.autojob.util.bean.ObjectUtil;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author JingGe(* ^ ▽ ^ *)
 * @date 2023-09-11 15:45
 * @email 1158055613@qq.com
 */
public class MapBuilder {
    private final Map<String, Object> params = new HashMap<>();

    private MapBuilder() {
    }

    public static MapBuilder newInstance() {
        return new MapBuilder();
    }

    public MapBuilder addEntry(String name, Object value) {
        params.put(name, value);
        return this;
    }

    public MapBuilder addAllEntries(Map<String, Object> params) {
        this.params.putAll(params);
        return this;
    }

    public MapBuilder addObjectEntries(Object bean) {
        if (ObjectUtil.isNull(bean)) {
            return this;
        }
        Field[] fields = ObjectUtil.getObjectFields(bean);
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(bean);
                if (value == null) {
                    continue;
                }
                params.put(field.getName(), value);
            } catch (Exception ignored) {
            }
        }
        return this;
    }

    public Map<String, Object> getMap() {
        return this.params;
    }

    public MapBuilder clear() {
        params.clear();
        return this;
    }
}
